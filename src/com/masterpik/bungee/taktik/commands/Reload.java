package com.masterpik.bungee.taktik.commands;

import com.masterpik.bungee.taktik.events.ChatEvents;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

public class Reload extends Command{
    public Reload(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        if (args != null
                && args.length >= 1) {
            if (args[0].equalsIgnoreCase("reload")) {
                ChatEvents.reload();
            }
        }

    }
}
