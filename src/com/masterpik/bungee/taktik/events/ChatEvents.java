package com.masterpik.bungee.taktik.events;

import com.masterpik.bungee.taktik.Taktik;
import com.masterpik.messages.bungee.Api;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;

import java.io.File;
import java.io.IOException;
import java.text.Collator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class ChatEvents implements Listener{

    public static Configuration generalConfy;
    public static File generalFile;

    public static ArrayList<String> badChats;
    public static ArrayList<String> passBadChats;

    public static String badChat;
    public static String spam;

    public static HashMap<ProxiedPlayer, String> cashCmds;

    public static void chatEventsInit() {
        badChat = Taktik.prefix+Api.getString("noCheat.chat.badChat");
        spam = Taktik.prefix+Api.getString("noCheat.chat.spam");

        ProxyServer.getInstance().getPluginManager().registerListener(Taktik.plugin, new ChatEvents());

        badChats = new ArrayList<>();
        passBadChats = new ArrayList<>();
        cashCmds = new HashMap<>();

        passBadChats.add("-");
        passBadChats.add(":");
        passBadChats.add(";");
        passBadChats.add(",");
        passBadChats.add(".");
        passBadChats.add("(");
        passBadChats.add(")");
        passBadChats.add("?");
        passBadChats.add(">");
        passBadChats.add("<");
        passBadChats.add("{");
        passBadChats.add("}");
        passBadChats.add("[");
        passBadChats.add("]");
        passBadChats.add("\"");
        passBadChats.add("'");
        passBadChats.add("#");
        passBadChats.add("@");
        passBadChats.add("_");
        passBadChats.add("!");
        passBadChats.add("€");
        passBadChats.add("$");
        passBadChats.add("*");
        passBadChats.add("/");
        passBadChats.add("\\");
        passBadChats.add("+");
        passBadChats.add("=");
        passBadChats.add("£");
        passBadChats.add("%");
        passBadChats.add(" ");

        try {
            if (!Taktik.plugin.getDataFolder().exists()) {
                Taktik.plugin.getDataFolder().mkdir();
            }

            generalFile = new File(Taktik.plugin.getDataFolder().getPath(), "badChat.yml");

            if (!generalFile.exists()) {
                generalFile.createNewFile();
                generalConfy = ConfigurationProvider.getProvider(YamlConfiguration.class).load(generalFile);

                ArrayList<String> init = new ArrayList<>();
                init.add("salaud");
                init.add("con");

                badChats.addAll(init);

                generalConfy.set("BadChat.list", init);

                saveFile(generalConfy, generalFile);
            }
            else {
                generalConfy = ConfigurationProvider.getProvider(YamlConfiguration.class).load(generalFile);

                badChats = getBadChats();

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void PlayerChatEvent(ChatEvent event) {

        if (event.getSender() instanceof ProxiedPlayer) {
            ProxiedPlayer player = (ProxiedPlayer) event.getSender();
            String message = event.getMessage();

            if (message.toCharArray()[0] != '/') {

                if (isBadChat(message, player.getName())) {
                    event.setCancelled(true);
                    player.sendMessage(new TextComponent(badChat));
                } else {
                    if (cashCmds.containsKey(player)) {
                        if (cashCmds.get(player).equalsIgnoreCase(message)) {
                            event.setCancelled(true);
                            player.sendMessage(new TextComponent(spam));
                        } else {
                            cashCmds.remove(player);
                            cashCmds.put(player, message);
                        }
                    } else {
                        cashCmds.put(player, message);
                    }

                    final String finalMessage = message;
                    ProxyServer.getInstance().getScheduler().schedule(Taktik.plugin, new Runnable() {
                        @Override
                        public void run() {
                            if (cashCmds.containsKey(player)) {
                                if (cashCmds.get(player).equalsIgnoreCase(finalMessage)) {
                                    cashCmds.remove(player);
                                }
                            }
                        }
                    }, 1, TimeUnit.MINUTES);
                }

            }

        }

    }


    public static boolean isBadChat(String message, String player) {
        boolean bad = false;
        String strBad = "null";

        message = message.toLowerCase();

        for (String ch : badChats) {
            if (message.contains(ch.toLowerCase())) {
                if (message.toCharArray().length == ch.length()) {
                    strBad = ch;
                    bad = true;
                    break;
                } else if (message.startsWith(ch)) {
                    if (message.length() > ch.length()) {
                        if (passBadChats.contains(Character.toString(message.toCharArray()[ch.length()]))) {
                            strBad = ch;
                            bad = true;
                            break;
                        }
                    }
                } else if (message.endsWith(ch)) {
                    if (message.length() > ch.length()) {
                        if (passBadChats.contains(Character.toString(message.toCharArray()[(message.length() - ch.length()) - 1]))) {
                            strBad = ch;
                            bad = true;
                            break;
                        }
                    }
                } else if (message.toCharArray().length > ch.length()) {
                    String[] decop = message.split(ch);
                    if (decop != null
                            && decop.length >= 2) {
                        if (passBadChats.contains(Character.toString(decop[0].toCharArray()[decop[0].length() - 1]))
                                && passBadChats.contains(Character.toString(decop[1].toCharArray()[0]))) {
                            strBad = ch;
                            bad = true;
                            break;
                        }
                    }
                }
            }
        }

        if (bad
                && !player.equalsIgnoreCase("null")) {
            ProxyServer.getInstance().getLogger().info("[TAKTIK] BadChat = (player : "+player+" | BadWord : "+strBad+")");
        }

        return bad;
    }

    public static boolean isBadChat(String message) {
        return isBadChat(message, "null");
    }


    public static ArrayList<String> getBadChats() {
        return (ArrayList<String>) generalConfy.getList("BadChat.list");
    }

    public static void saveFile(Configuration file, File fileg) {
        try {
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(file, fileg);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void reload() {
        try {
            generalConfy = ConfigurationProvider.getProvider(YamlConfiguration.class).load(generalFile);

            badChats = getBadChats();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
