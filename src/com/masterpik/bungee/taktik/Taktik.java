package com.masterpik.bungee.taktik;

import com.masterpik.bungee.taktik.commands.Reload;
import com.masterpik.bungee.taktik.events.ChatEvents;
import com.masterpik.messages.bungee.Api;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;

public class Taktik extends Plugin {

    public static Plugin plugin;

    public static String prefix;

    public void onEnable() {
        plugin = this;
        prefix = Api.getString("noCheat.chat.mainPrefix");
        ChatEvents.chatEventsInit();
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new Reload("btkt"));


    }

}
